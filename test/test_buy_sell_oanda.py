import sys
import os
sys.path.append('../shared')
sys.path.append('../sources-sink-oanda')
from logger import LOGGER
from market_factory import MarketFactory
import time

if __name__ == "__main__":
    logging = LOGGER.getInstance(os.environ['TEST_FILE_NAME'])
    logging.debug('Start Test')
    logging.debug('ENVIRONMENT: ',os.environ['ENVIRONMENT'])
    
    market = MarketFactory.getInstance({})

    time.sleep(3)
    market.BUY({})
    time.sleep(20)
    market.SELL({})
    
    #market.GET_STREAM({}).subscribe(lambda s: logging.debug(s))
    #
    time.sleep(5*60)
    sys.exit()
