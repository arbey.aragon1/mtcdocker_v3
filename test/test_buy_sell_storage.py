import sys
import os
sys.path.append('../shared')
sys.path.append('../shared/storage-factory')
from logger import LOGGER
from storage_factory import StorageFactory
import time


if __name__ == "__main__":
    logging = LOGGER.getInstance(os.environ['TEST_FILE_NAME'])
    logging.debug('Start Test')
    logging.debug('ENVIRONMENT: ',os.environ['ENVIRONMENT'])
    
    market = StorageFactory.getInstance({})
    market.BUY({'testBUY':'test'})
    market.SELL({'testSELL':'test'})
    