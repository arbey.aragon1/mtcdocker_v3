import sys
import os
sys.path.append('../shared')
from logger import LOGGER
import time
from rx import Observable, Observer
import threading
from mongo_service import MongoService
import numpy as np

d1=None
if __name__ == "__main__":
    logging = LOGGER.getInstance(os.environ['TEST_FILE_NAME'])
    logging.debug('Start Test reconnection')

    def RECONNECT():
        logging.debug('Reinicia conexion')
        if d1!=None:
            d1.dispose()
        startConnection()
        return None

    def startConnection():
        d1 = Observable.interval(1000) \
            .filter(lambda s: np.random.choice([True, False], p=[0.6,0.4])) \
            .switch_map(lambda s: Observable.timer(3000) \
                                            .do_action(lambda x: RECONNECT())
                                            .start_with(s) ) \
            .do_action(lambda s: logging.debug(s)).subscribe(
                on_error = lambda e: (logging.debug("Error Occurred subscribe: {0}".format(e)), RECONNECT()),
                on_completed = lambda: (logging.debug("Done!"), RECONNECT()))

    startConnection()
    while(True):
        time.sleep(10)