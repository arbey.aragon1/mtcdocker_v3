import sys
import os
sys.path.append('../shared')
sys.path.append('../shared/firestore-factory')
from logger import LOGGER
from firestore_factory import FirestoreFactory
import time
from rx import Observable, Observer

if __name__ == "__main__":
    logging = LOGGER.getInstance(os.environ['TEST_FILE_NAME'])
    logging.debug('Start Test')
    logging.debug('ENVIRONMENT: ',os.environ['ENVIRONMENT'])
    
    fs = FirestoreFactory.getInstance()
    
    docs = fs.GET_DATA_BY_LIMIT('history', 'time',65*15)
    
    def mainStream(observer):        
        for i,doc in enumerate(docs):
            if i%15 == 0:
                observer.on_next(doc.to_dict())
        observer.on_completed()
    stream=Observable.create(mainStream)

    newStream=Observable.interval(2)

    stream = Observable.concat(stream, newStream)

    stream.subscribe(on_next = lambda s:logging.debug('data :', s),
        on_error = lambda e: logging.debug("Error Occurred: {0}".format(e)),
        on_completed = lambda: logging.debug("completed"))

    time.sleep(5)
