import sys
import os
sys.path.append('../shared')
from logger import LOGGER
import time
from rx import Observable, Observer

from mongo_service import MongoService

if __name__ == "__main__":

    time.sleep(4)
    logging = LOGGER.getInstance(os.environ['TEST_FILE_NAME'])
    logging.debug('Start Test')
    logging.debug('ENVIRONMENT: ',os.environ['ENVIRONMENT'])
    logging.debug('------1')
    mongo = MongoService.getInstance()
    logging.debug('------2')
    mongo.updateDoc('brent', 'currentValue', {'name':'test','data': 55})
    logging.debug('------3')
    logging.debug(mongo.getDoc('brent'))
    logging.debug('------4')