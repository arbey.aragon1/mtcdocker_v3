import sys
import os
sys.path.append('../shared')
sys.path.append('../shared/firestore-factory')
from logger import LOGGER
from firestore_factory import FirestoreFactory
import time

if __name__ == "__main__":
    logging = LOGGER.getInstance(os.environ['TEST_FILE_NAME'])
    logging.debug('Start Test')
    logging.debug('ENVIRONMENT: ',os.environ['ENVIRONMENT'])
    
    fs = FirestoreFactory.getInstance()
    fs.ADD_DATA('brent',{'test':'test'})