import sys
import os
sys.path.append('../shared')
sys.path.append('../shared/storage-factory')
sys.path.append('../algorithms')
from logger import LOGGER
from storage_factory import StorageFactory

if __name__ == "__main__":
    logging = LOGGER.getInstance(os.environ['TEST_FILE_NAME'])
    
    config={
        'fileDataName':'data.csv',
        'fileDataNameOut':'dataOut.csv',
        'fileOrderName':'orders.csv',
        'fileOrlogger': False
        }
    test = StorageFactory.getInstance(config)
    for i in range(10):
        test.DATA_IN_SAVE({'first_name': 1, 'last_name': 'Beans', 'tt':3})
        test.BUY({'first_name': 5, 'last_name': 'Beans', 'tt':10})
        test.DATA_OUT_SAVE({'first_name': 8, 'last_name': 'Beans', 'tt':5})
        test.SELL({'first_name': 11, 'last_name': 'Beans', 'tt':4})

