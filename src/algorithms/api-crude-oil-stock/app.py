import sys
from sys import argv
import os
sys.path.append('../../shared')
sys.path.append('../../shared/firestore-factory')
import time
from datetime import datetime
from firestore_factory import FirestoreFactory
from redis_service import RedisService
from constants import TipeMSG, VOTE_OIL, API_OIL_CRUDE_STOCK_STATUS, ACTIVATION, BUY, SELL, API_OIL_CRUDE_STOCK_ID
from logger import LOGGER
from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from datetime import date

class WORK():
  STARTED = 'STARTED'
  FINALIZED = 'FINALIZED'

if __name__ == "__main__":

    logging = LOGGER.getInstance(path='../api-crude-oil-stock/logs', mode='a')
    logging.debug('Start')
    sink = RedisService(VOTE_OIL)
    
    if argv[1] == ACTIVATION.ACTIVATE:
        #driver = webdriver.Remote("http://selenium-hub:4444/wd/hub", DesiredCapabilities.CHROME)
        #driver.get('https://www.fxstreet.com/economic-calendar/event/bcf389bd-5fe2-4bd1-b1e0-d0b3910ba944')
        #text = driver.page_source.replace('\n','').replace('\t','').replace('\r','').replace(' ','')
        #value = float(text.split('fxst-actual-value')[1].split('>')[1].split('M')[0])
#
        #dateValidator = driver.find_element_by_class_name('fxst-nextevent') \
        #    .find_element_by_class_name('fxst-date-value') \
        #    .text.split(',')[1].split('\n')[0] \
        #    .split(' ')[2]
#
        #today = date.today()
        #d1 = int(today.strftime("%d"))
        #d2 = int(dateValidator)
#
        #driver.close()
#
        #logging.debug(value)
        #logging.debug(d1)
        #logging.debug(d2)
        #logging.debug(d2 == d1)
        d1 = 1
        d2 = 1
        value = 1


        configOrder = {
            'type': TipeMSG.INFO,
            'id-al': API_OIL_CRUDE_STOCK_ID, 
            'action': ACTIVATION.ACTIVATE
        }
        sink.updateMessage(configOrder)
        
        configOrder = {}
        if d2 == d1:
            if value >= 0:
                #sell
                configOrder['type'] = TipeMSG.ORDER
                configOrder['id-al'] = API_OIL_CRUDE_STOCK_ID
                configOrder['order'] = SELL
                sink.updateMessage(configOrder)

            else:
                #buy
                configOrder['type'] = TipeMSG.ORDER
                configOrder['id-al'] = API_OIL_CRUDE_STOCK_ID
                configOrder['order'] = BUY
                sink.updateMessage(configOrder)

    elif argv[1] == ACTIVATION.DESACTIVATE:
        configOrder = {
            'type': TipeMSG.INFO,
            'id-al': API_OIL_CRUDE_STOCK_ID, 
            'action': ACTIVATION.DESACTIVATE
        }
        sink.updateMessage(configOrder)




