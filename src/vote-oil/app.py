import sys
import os
sys.path.append('../shared')
from logger import LOGGER
import time
from constants import VOTE_OIL, VOTE_TO_OANDA, TipeMSG, ECHO_DELTA_TIME, ACTIVATION
from constants import MOVING_AVERAGES_ID, API_OIL_CRUDE_STOCK_ID
from constants import ANY, BUY, SELL
from random import randrange
from rx import Observable, Observer
import threading
from redis_service import RedisService

weightsDefault = {
    [MOVING_AVERAGES_ID]: 1.0,
    [API_OIL_CRUDE_STOCK_ID]: 0.0,
}

if __name__ == "__main__":

    weights = weightsDefault
    votes = {
        [MOVING_AVERAGES_ID]: ANY,
        [API_OIL_CRUDE_STOCK_ID]: ANY,
    }

    ##Instancia de logger
    logging = LOGGER.getInstance()
    logging.debug('Start! ', os.environ['ENVIRONMENT'])

    ##Instancia para el envio del resultado como orden a oanda
    ##Donde llegan los votos de cada algoritmo
    source_sink = RedisService()

    ##Donde proceso los votos
    def nextFunction(s):
        logging.debug('------------',s)
        ##Envia orden a oanda
        if s['type']==TipeMSG.ECHO:
            logging.debug('Echo')

        elif s['type']==TipeMSG.ORDER:
            logging.debug('Out vote: ',s)
            votes[s['id-al']] = s['order']

            #source_sink.updateMessage(s, channel=VOTE_TO_OANDA)

        elif s['type']==TipeMSG.INFO:

            if(s['id-al'] == API_OIL_CRUDE_STOCK_ID):
                logging.debug('WEIGHT NORM: ',s)
                if(s['action'] == ACTIVATION.ACTIVATE):
                    weights = {
                        [MOVING_AVERAGES_ID]: 0.5,
                        [API_OIL_CRUDE_STOCK_ID]: 0.5,
                    }

                elif(s['action'] == ACTIVATION.DESACTIVATE):
                    weights = weightsDefault
                    votes[API_OIL_CRUDE_STOCK_ID]=ANY 

        
                    
    flowData = source_sink.getFlow(VOTE_OIL) \
            .do_action(lambda s: nextFunction(s))
    
    echo = Observable.interval(ECHO_DELTA_TIME*1000) \
            .start_with({}) \
            .do_action(lambda s: \
                (logging.debug('Send echo'), source_sink.updateMessage({'type': TipeMSG.ECHO}, channel=VOTE_TO_OANDA)))

    def thread_function():
        #echo,
        Observable.merge(flowData, echo).subscribe(
            on_error = lambda e: (logging.debug("Error Occurred subscribe: {0}".format(e)), sys.exit()),
            on_completed = lambda: (logging.debug("Done!"), sys.exit()))
    
    x = threading.Thread(target=thread_function)
    x.start()

    ##Inicio el consumo
    logging.debug('Start (vote-oil)')

    while(True):
        time.sleep(60)

