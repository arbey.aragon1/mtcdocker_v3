import sys
import os
sys.path.append('../shared')
sys.path.append('../algorithms')
sys.path.append('../shared/firestore-factory')
from app import Algorithm
from logger import LOGGER
from constants import VOTE_OIL, TipeMSG, ECHO_DELTA_TIME, MARKET_HOUR_ACTIVATION, ACTIVATION
import time
from rx import Observable, Observer
from simulation_streams import streamRandomWalk, streamHistData
from firestore_factory import FirestoreFactory
import threading
from redis_service import RedisService

ENVIRONMENT=os.environ['ENVIRONMENT']
STAGING=os.environ['STAGING']
PRODUCTION=os.environ['PRODUCTION']

INTERVAL = 1
if ENVIRONMENT == STAGING:
    DELTA_TIME = 100
elif ENVIRONMENT == PRODUCTION:
    DELTA_TIME = 15*60*1000

if __name__ == "__main__":

    logging = LOGGER.getInstance()
    firestore = FirestoreFactory.getInstance()
    logging.debug('Start! ', ENVIRONMENT)
    sink = RedisService(VOTE_OIL)
    
    def getData():
        return sink.getMSG('brent')

    contectionBreak = False
    for i in range(60):
        data = getData()
        if data != None:
            contectionBreak = False
            break
        else:
            logging.debug('Error en lectura brent intento: ', i)
            contectionBreak = True
            time.sleep(3)

    if contectionBreak:
        logging.debug('Conexion perdida')

    inputFlow = Observable.interval(DELTA_TIME) \
        .filter(lambda s: sink.getMSG(MARKET_HOUR_ACTIVATION) == ACTIVATION.ACTIVATE) \
        .map(lambda s: getData())

    module = int(os.environ['FIRESTORE_READ_MOD'])
    docs = [doc.to_dict() for doc in firestore.GET_DATA_BY_LIMIT('history', 'time', (int(os.environ['ML'])+1)*module)]
    docs.reverse()
    def mainStream(observer):
        for i,doc in enumerate(docs):
            if i%module == 0:
                observer.on_next(doc)
        observer.on_completed()

    streamHistory=Observable.create(mainStream)

    inputFlow = Observable.concat(streamHistory, inputFlow)

    configDB = {
        'fileDataName':'dataInput.csv',
        'fileDataNameOut':'dataOut.csv',
        'fileOrderName':'orders.csv',
        'fileOrlogger': True
        }
    algo = Algorithm({'interval': INTERVAL},
        inputFlow,
        configDB)

    newFlow = algo.flowConnect()
    
    def sendOrder(d):
        logging.debug('Order: ',d)
        sink.updateMessage(d)

    newOrderFlow = algo.getOrderFlow() \
            .do_action(lambda s: sendOrder(s))

    echo = Observable.interval(ECHO_DELTA_TIME*1000) \
            .start_with({}) \
            .do_action(lambda s: \
                (logging.debug('Send echo'), sink.updateMessage({'type': TipeMSG.ECHO})))
        
    def thread_function():
        Observable.merge(echo,newOrderFlow,newFlow).subscribe(
            on_error = lambda e: logging.debug("Error Occurred subscribe: {0}".format(e)),
            on_completed = lambda: (logging.debug("Done!"), sys.exit()))
    
    x = threading.Thread(target=thread_function)
    x.start()

    while(True):
        time.sleep(60)
