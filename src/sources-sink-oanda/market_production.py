from market_interface import MarketInterface
import os
import json
from oandapyV20 import API
from oandapyV20.exceptions import V20Error
from oandapyV20.endpoints.pricing import PricingStream
from rx import Observable, Observer
import configparser
import datetime
import oandapyV20.endpoints.orders as orders
from logger import LOGGER
import urllib3
import requests
import time
    
class MarketProduction(MarketInterface):
    __instance = None
    __api = None
    __accountID = None
    __access_token = None
    __logger = None
    __instrument = None
    __observer = None
    __source = None

    def __init__(self, config):
        if MarketProduction.__instance != None:
            raise Exception("Esta clase no es singleton!")
        else:
            MarketProduction.__instance = self
            MarketProduction.__config = config
            self.__logger = LOGGER.getInstance()
            self.__instrument = "BCO_USD"
            MarketProduction.setup()
            
    @staticmethod
    def setup():
        requests.packages.urllib3.disable_warnings()
        requests.packages.urllib3.util.ssl_.DEFAULT_CIPHERS += 'HIGH:!DH:!aNULL'
        try:
            requests.packages.urllib3.contrib.pyopenssl.DEFAULT_SSL_CIPHER_LIST += 'HIGH:!DH:!aNULL'
        except AttributeError:
            pass

        MarketProduction.__accountID = os.environ['ACCOUNT_ID']
        MarketProduction.__access_token = os.environ['API_KEY']

    @staticmethod
    def getInstance(config):
        if MarketProduction.__instance == None:
            MarketProduction(config)
        return MarketProduction.__instance 

    def __initStream(self):
        self.__logger.debug('Market production mode')

        def dataToObj(R):
            bid = float(R['bids'][0]['price'])
            ask = float(R['asks'][0]['price'])
            timeStamp = R['time']
            datetime_object = datetime.datetime.strptime(timeStamp[:-4], '%Y-%m-%dT%H:%M:%S.%f')
            return {
                'time': timeStamp,
                'bid': bid, 
                'ask': ask,
                'price': bid,
                'day': datetime_object.day
                }

        def mainStream(observer):
            try:
                s = PricingStream(
                    accountID = MarketProduction.__accountID, 
                    params={
                        "instruments": self.__instrument
                        })
                for R in MarketProduction.__api.request(s):
                    observer.on_next(R)
            except Exception as e:
                self.__logger.debug("Error MarketProduction.__api.request: {}".format(e))
                observer.on_error(e)

        self.__source =  Observable.create(mainStream) \
            .filter(lambda s: s['type'] == 'PRICE') \
            .map(lambda s: dataToObj(s)) \

    def GET_STREAM(self, data):
        MarketProduction.__api = API(
                access_token = MarketProduction.__access_token, 
                environment="live")
        self.__initStream()
        return self.__source

    def BUY(self, order):
        if (os.environ['CAN_SEND_ORDERS'] == 'True'):
            data = {
                'order':{
                    "timeInForce": "FOK",
                    "instrument": self.__instrument,
                    "units": "3",
                    "type": "MARKET",
                    "positionFill": "DEFAULT"
                }
            }
            r = orders.OrderCreate(
                MarketProduction.__accountID, 
                data=data)
            MarketProduction.__api.request(r)
        else:
            self.__logger.debug('+++BUY_ORDER+++')
        
    def SELL(self, order):
        if (os.environ['CAN_SEND_ORDERS'] == 'True'):
            data = {
                'order':{
                    "timeInForce": "FOK",
                    "instrument": self.__instrument,
                    "units": "-3",
                    "type": "MARKET",
                    "positionFill": "DEFAULT"
                }
            }
            r = orders.OrderCreate(
                MarketProduction.__accountID, 
                data=data)
            MarketProduction.__api.request(r)
        else:
            self.__logger.debug('---SELL_ORDER---')