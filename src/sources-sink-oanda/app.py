import sys
import os
sys.path.append('../shared')
from logger import LOGGER
import time
from constants import OANDA_OIL, VOTE_TO_OANDA
import numpy as np
from price_data import PriceData
from datetime import datetime
from market_factory import MarketFactory
from constants import LONG, SHORT, ANY, BUY, SELL, TipeMSG, SLEEP_TO_KILL
from rx import Observable, Observer
import threading
from redis_service import RedisService
import numpy as np

d1=None
if __name__ == "__main__":

    #Instancia de logger
    logging = LOGGER.getInstance()  

    #Intancia de mercado
    market = MarketFactory.getInstance({})
    
    logging.debug('Start! ', os.environ['ENVIRONMENT'])
    i=0
    value=50

    ##Instancia la entrada de ordenes
    source = RedisService(VOTE_TO_OANDA)
    
    ##Donde llegan las ordenes
    def getOrder(s):
        if s['type']==TipeMSG.ORDER:
            logging.debug('Order',s)
            if s['order'] == BUY:
                market.BUY({})
            elif s['order'] == SELL:
                market.SELL({})    
        elif s['type']==TipeMSG.ECHO:
            logging.debug('Echo')

    ##Donde son enviados los datos del streaming de oanda
    def sendValues(s):
        d=PriceData('brent', s['ask'], s['bid'], str(5), s['time'])
        source.setMSG(d.getDict(), 'brent')

    
    def RECONNECT():
        time.sleep(3)
        logging.debug('Reinicia conexion')
        if d1!=None:
            d1.dispose()
        startConnection()
        return None


    def startConnection():
        flowData = source.getFlow().do_action(lambda s: getOrder(s))
        #.filter(lambda s: np.random.choice([True, False], p=[0.9,0.1])) \
        marketStream = market.GET_STREAM({}) \
            .do_action(lambda s: sendValues(s)) \
            .switch_map(lambda s: Observable.timer(SLEEP_TO_KILL*1000) \
                                .do_action(lambda x: RECONNECT())
                                .start_with(s) ) \

        d1 = Observable.merge(flowData, marketStream).subscribe(
            on_error = lambda e: (logging.debug("Error Occurred subscribe: {0}".format(e)), RECONNECT()),
            on_completed = lambda: (logging.debug("Done!"), RECONNECT()))
        
    startConnection()
    while(True):
        time.sleep(10)