import sys
sys.path.append('../shared')
from logger import LOGGER
from market_interface import MarketInterface
from simulation_streams import streamRandomWalk
from datetime import datetime

class MarketSimulation(MarketInterface):
    __instance = None
    __logger = None

    def __init__(self, config):
        if MarketSimulation.__instance != None:
            raise Exception("Esta clase no es singleton!")
        else:
            MarketSimulation.__instance = self
            self.__logger = LOGGER.getInstance()

    @staticmethod
    def getInstance(config):
        if MarketSimulation.__instance == None:
            MarketSimulation(config)
        return MarketSimulation.__instance 

    
    def GET_STREAM(self, configST):
        if(True):
            self.__logger.debug('GET_STREAM')
            self.__logger.debug('Simulation market')
            return streamRandomWalk(
                mlSeconds=500, 
                initVal=50, 
                mu=1, 
                sigma=0.5,
                limitData=9999).map(lambda s: { 'time': datetime.now().strftime('%Y-%m-%dT%H:%M:%S.%f'),
                    'bid': s['price'], 
                    'ask': s['price'],
                    'price': s['price'],
                    'day': datetime.now().day
                })
        else:
            #return streamHistData(
            #    fileName='data.csv')
            return None

    def BUY(self, order):
        self.__logger.debug('++BUY++')

    def SELL(self, order):
        self.__logger.debug('--SELL--')