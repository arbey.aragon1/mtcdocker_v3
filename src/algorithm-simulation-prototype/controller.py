import sys
import os
sys.path.append('../shared')
sys.path.append('../algorithms')
from input_stream import InputStream
from app import Algorithm
from logger import LOGGER
import time

ENVIRONMENT=os.environ['ENVIRONMENT']
STAGING=os.environ['STAGING']
PRODUCTION=os.environ['PRODUCTION']
SIMULATION=os.environ['SIMULATION']

configDB = {
    'fileDataName':'dataInput.csv',
    'fileDataNameOut':'dataOut.csv',
    'fileOrderName':'orders.csv',
    'fileOrlogger': True
    }
    
INTERVAL = 1

if __name__ == "__main__":
    
    logging = LOGGER.getInstance()
    logging.debug('Start! ', ENVIRONMENT)

    inputInstance = InputStream.getInstance()
    inputFlow = inputInstance.GET_STREAM({})

    algo = Algorithm({'interval': INTERVAL},
        inputFlow,
        configDB)

    newFlow = algo.flowConnect()
    newOrderFlow = algo.getOrderFlow()

    newOrderFlow.subscribe(lambda s: logging.debug('*Order out: ',s))
    
    def completed():
        logging.debug("Done!")
        sys.exit()
    
    newFlow.subscribe(
        on_error = lambda e: logging.debug("Error Occurred: {0}".format(e)),
        on_completed = lambda: completed())

    while(True):
        time.sleep(60)
