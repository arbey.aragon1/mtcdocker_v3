import sys
sys.path.append('../shared')
from logger import LOGGER
from simulation_streams import streamRandomWalk, streamHistData
from datetime import datetime

class InputStream():
    __instance = None
    __logger = None

    def __init__(self):
        if InputStream.__instance != None:
            raise Exception("Esta clase no es singleton!")
        else:
            InputStream.__instance = self
            self.__logger = LOGGER.getInstance()
            
    @staticmethod
    def getInstance():
        if InputStream.__instance == None:
            InputStream()
        return InputStream.__instance 

    
    def GET_STREAM(self, configST):
        self.__logger.debug('GET_STREAM')
        if(False):
            return streamRandomWalk(
                mlSeconds=1000, 
                initVal=50, 
                mu=1, 
                sigma=0.5,
                limitData=9999).map(lambda s: { 'time': datetime.now().strftime('%Y-%m-%dT%H:%M:%S.%f'),
                    'bid': s['price'], 
                    'ask': s['price'],
                    'price': s['price'],
                    'day': datetime.now().day
                })
        else:
            return streamHistData(mlSeconds=None).map(lambda s: { 'time': s['time'],
                    'bid': s['price'], 
                    'ask': s['price'],
                    'price': s['price'],
                    'day': s['day']
                })