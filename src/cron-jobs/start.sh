#!/bin/bash

rm /home/project/logs/cron.log
touch /home/project/logs/cron.log

echo "Load Cron Jobs..."
rm cronjobs
python app.py

echo "Load Cron Jobs to crontab..."
crontab cronjobs
rm cronjobs >/dev/null 2>&1
cron >/dev/null 2>&1

echo "Show task"
crontab -l
echo "End"

(cd /home/project/ ; python server.py)
#(cd /home/algorithms/api-crude-oil-stock/ ; python app.py)
