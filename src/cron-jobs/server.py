import sys
import os
sys.path.append('../shared')
sys.path.append('../shared/storage-factory')
sys.path.append('../shared/firestore-factory')

from logger import LOGGER
import time
from rabbit_service import RabbitService
from constants import VOTE_OIL, VOTE_TO_OANDA, ACTIVATION, MARKET_HOUR_ACTIVATION
from random import randrange
from redis_service import RedisService
from firestore_factory import FirestoreFactory
from websocket_service import WebsocketService
from rx import Observable, Observer

ENVIRONMENT=os.environ['ENVIRONMENT']
STAGING=os.environ['STAGING']
PRODUCTION=os.environ['PRODUCTION']
SIMULATION=os.environ['SIMULATION']

if __name__ == "__main__":

    logging = LOGGER.getInstance(os.environ['TEST_FILE_NAME'])
    sink = RedisService()
    logging.debug('Start server cronjobs! ', os.environ['ENVIRONMENT'])

    if ENVIRONMENT == PRODUCTION:
        instance = FirestoreFactory.getInstance()
    else:
        instance = WebsocketService.getInstance()
    
    def response(req):
        currentValue = sink.getMSG(MARKET_HOUR_ACTIVATION)
        logging.debug("Current status :", currentValue)
        logging.debug(req)

        if req['action'] == 'RUN_OIL_STOCKS_AL':
            logging.debug('RUN_OIL_STOCKS_AL')
            os.system("(cd /home/algorithms/api-crude-oil-stock/ ; python app.py ACTIVATE >> /home/project/logs/cron.log) 2>\&1 >/dev/null")
        elif req['action'] == 'STOP_OIL_STOCKS_AL':
            logging.debug('STOP_OIL_STOCKS_AL')
            os.system("(cd /home/algorithms/api-crude-oil-stock/ ; python app.py DESACTIVATE >> /home/project/logs/cron.log) 2>\&1 >/dev/null")
        
    instance.GET_TASK_QUEUE() \
        .do_action(lambda req: logging.debug('Task: ',req)) \
        .do_action(lambda req: response(req)) \
        .subscribe(
            on_error = lambda e: logging.debug("Error Occurred subscribe: {0}".format(e)),
            on_completed = lambda: (logging.debug("Done!"), sys.exit()))

    instance.START()

    while(True):
        time.sleep(60)
