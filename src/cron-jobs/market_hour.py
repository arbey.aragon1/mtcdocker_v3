import sys
from sys import argv
import os
sys.path.append('../shared')
sys.path.append('../shared/firestore-factory')
import time
from datetime import datetime
from firestore_factory import FirestoreFactory
from redis_service import RedisService
from constants import MARKET_HOUR_ACTIVATION

if __name__ == "__main__":
    sink = RedisService()
    sink.setMSG(argv[1], MARKET_HOUR_ACTIVATION)
    print(argv[1], datetime.now().strftime('%Y-%m-%dT%H:%M:%S.%f'))
    