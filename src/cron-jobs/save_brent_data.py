import sys
import os
sys.path.append('../shared')
sys.path.append('../shared/firestore-factory')
import time
from datetime import datetime
from firestore_factory import FirestoreFactory
from redis_service import RedisService
from constants import MARKET_HOUR_ACTIVATION, ACTIVATION

if __name__ == "__main__":
    
    firestore = FirestoreFactory.getInstance()
    source = RedisService()

    s = source.getMSG('brent')
    hourValid = (source.getMSG(MARKET_HOUR_ACTIVATION) == ACTIVATION.ACTIVATE)
    if (s != None) and hourValid:
        firestore.ADD_DATA('history', s)
        #print(s, datetime.now().strftime('%Y-%m-%dT%H:%M:%S.%f'))
    else:
        print('Error en lectura brent')        
    