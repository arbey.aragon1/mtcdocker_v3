import sys
import os
sys.path.append('../shared')
sys.path.append('../algorithms')
from logger import LOGGER
import time
from rabbit_service import RabbitService
from constants import VOTE_OIL, VOTE_TO_OANDA, ACTIVATION, MARKET_HOUR_ACTIVATION
from random import randrange
from redis_service import RedisService

if __name__ == "__main__":
    ##Instancia de logger
    logging = LOGGER.getInstance()
    logging.debug('Start! ', os.environ['ENVIRONMENT'])
    fs = open('cronjobs', 'w')
    for key in os.environ.keys():
        logging.debug(key, os.environ[key])
        fs.write(key+'='+os.environ[key]+'\n')
    
    def commandCreateWithPath(crobjob,command, path):
        try:
            os.remove(path+'logs/log.log')
        except:
            pass
        return crobjob+" (cd "+path+" ; "+command+" >> /home/project/logs/cron.log) 2>\&1 >/dev/null \n"

    def commandCreate(crobjob,command):
        return commandCreateWithPath(crobjob, command, "/home/project/")
    
    sink = RedisService()
    sink.setMSG(ACTIVATION.ACTIVATE, MARKET_HOUR_ACTIVATION)

    fs.write(commandCreate('* * * * *','python save_brent_data.py'))
    
    fs.write(commandCreate('0 17 * * MON-THU','python market_hour.py '+ACTIVATION.DESACTIVATE))
    fs.write(commandCreate('0 19 * * MON-THU','python market_hour.py '+ACTIVATION.ACTIVATE))

    fs.write(commandCreate('0 19 * * SUN','python market_hour.py '+ACTIVATION.ACTIVATE))
    fs.write(commandCreate('0 16 * * FRI','python market_hour.py '+ACTIVATION.DESACTIVATE))

    fs.write(commandCreateWithPath('0 16 * * TUE', 'python app.py '+ACTIVATION.ACTIVATE, '/home/algorithms/api-crude-oil-stock/'))
    fs.write(commandCreateWithPath('0 16 * * WED', 'python app.py '+ACTIVATION.DESACTIVATE, '/home/algorithms/api-crude-oil-stock/'))
    
    fs.write('##\n')
    fs.close()
