firebase.initializeApp({
  apiKey: "AIzaSyDfq8UY5nh8zBN5pLR79azaGXec_07_7lE",
  authDomain: "mtcdb-912e6.firebaseapp.com",
  databaseURL: "https://mtcdb-912e6.firebaseio.com",
  projectId: "mtcdb-912e6",
  storageBucket: "mtcdb-912e6.appspot.com",
  messagingSenderId: "635396422651",
  appId: "1:635396422651:web:da8c4bbf2170dbcd"
});

var email = "test@test.com";
var password = "012345678";

var db = firebase.firestore();

function create_UUID(){
  var dt = new Date().getTime();
  var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = (dt + Math.random()*16)%16 | 0;
      dt = Math.floor(dt/16);
      return (c=='x' ? r :(r&0x3|0x8)).toString(16);
  });
  return uuid;
}

var dataUser = null

firebase
  .auth()
  .signInWithEmailAndPassword(email, password)
  .then(function(user) {
    console.log('data user:', user.user.uid)
    return firebase.auth().currentUser.getIdToken()
    .then((token)=>({'token':token, 'uid':user.user.uid}))
  })
  .then((dataUserResponse)=>{
    dataUser = dataUserResponse;
    document.getElementById("marketSwitch").disabled = false;
  })
  .catch(function(error) {
    console.error("Error adding document: ", error);
  });


function sendTask(command, dataToSend, refreshCallback){

  var dataToSend = {...{
    command: command,
    uuid: create_UUID(),
    uuid_ms: 'NOT-MS',
    uid_user: dataUser['uid'],
    token: dataUser['token'],
    time: (new Date()).getTime(),
  },...dataToSend}
  
  var refDocument = db
  .collection("microservice")
    .doc("echo")
    .collection("task");
    
    refDocument
    .add(dataToSend)    
    .then(function() {
      
      console.log('Request enviado')
      var ref = db
        .collection("microservice")
        .doc("echo")
        .collection(dataToSend['uid_user'])
        .doc(dataToSend['uuid'])

      var unsubscribe = ref
        .onSnapshot(function(snapshot) {
          if(snapshot.exists){
            var dataSnap = snapshot.data();
            console.log('Respuesta recivida: ', dataSnap)
            ref.delete().then(function() {
              console.log('Respuesta eliminada')
              refreshCallback(dataSnap);
            });
            unsubscribe();
          }
        });
    });
}

function testUpdateMarketHour(dataResponse){
  console.log('testUpdateMarketHour: ', dataResponse)
  document.getElementById('marketStatus').innerHTML = JSON.stringify(dataResponse);
}


