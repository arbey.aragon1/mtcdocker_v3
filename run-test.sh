sudo docker kill $(sudo docker ps -a -q)
sudo docker rm $(sudo docker ps -a -q)
sudo docker volume rm $(sudo docker volume ls -qf dangling=true | xargs)
sudo docker-compose -f docker-compose-test.yml build
sudo docker-compose -f docker-compose-test.yml up