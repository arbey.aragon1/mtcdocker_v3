import logging
import os
import sys

class LOGGER():
    __mtcdb = None
    __node = None
    __config = None
    __instance = None
    __logger = None
    __writer = None
    __loggerOn = True

    def __init__(self, fileName, path, mode):
        if LOGGER.__instance != None:
            raise Exception("Esta clase no es singleton!")
        else:
            LOGGER.__instance = self
            fileDir = path+'/'+fileName
            
            try:
                os.mkdir(path)
            except OSError:
                pass
            try:
                if mode == 'w':
                    os.remove(fileDir)
            except:
                pass
                
            def create_logger():
                # create logger for "Sample App"
                logger = logging.getLogger('automated_testing')
                logger.setLevel(logging.DEBUG)

                # create file handler which logs even debug messages
                fh = logging.FileHandler(fileDir, mode=mode)
                fh.setLevel(logging.DEBUG)

                # create console handler with a higher log level
                ch = logging.StreamHandler(stream=sys.stdout)
                ch.setLevel(logging.INFO)

                # create formatter and add it to the handlers
                formatter = logging.Formatter('%(asctime)s : %(levelname)s : %(message)s')
                fh.setFormatter(formatter)
                ch.setFormatter(formatter)

                # add the handlers to the logger
                logger.addHandler(ch)
                logger.addHandler(fh)

                return logger

            self.__writer = create_logger()
            self.__loggerOn = (os.environ['LOGGER'] == 'True') or (os.environ['ENVIRONMENT'] == os.environ['STAGING'])

    @staticmethod
    def getInstance(fileName='log.log', path='../project/logs', mode='w'):
        if LOGGER.__instance == None:
            LOGGER(fileName, path, mode)
        return LOGGER.__instance 

    def debug(self,*args,**kwargs):
        if self.__loggerOn:
            self.__writer.debug(args,**kwargs)