

import sys
sys.path.append('../shared')
from logger import LOGGER
import pika
import time
import os
from rx import Observable, Observer
import threading
import redis
import traceback

import json
import tornado.ioloop
import tornado.web
import tornado.websocket
import tornado.httpserver
import tornado.options

class SimpleWebSocket(tornado.websocket.WebSocketHandler):    
    __observer = None

    def open(self):
        SimpleWebSocket.logging = LOGGER.getInstance(os.environ['TEST_FILE_NAME'])
        SimpleWebSocket.logging.debug("WebSocket opened")
        SimpleWebSocket.__connected = True
        SimpleWebSocket.send = self.write_message

    def on_message(self, message):
        if SimpleWebSocket.__observer != None:
            SimpleWebSocket.__observer.on_next(json.loads(message))

    def on_close(self):
        SimpleWebSocket.logging.debug("WebSocket closed")

    def check_origin(self, origin):
        return True

    @classmethod
    def GET_TASK_QUEUE(cls):
        def mainStream(observer):
            SimpleWebSocket.__observer=observer
        return Observable.create(mainStream)

    @classmethod
    def RESPONSE(cls, req, resp):
        SimpleWebSocket.logging.debug(SimpleWebSocket.__connected)
        if SimpleWebSocket.__connected and (SimpleWebSocket.__connected != None):
            SimpleWebSocket.send(resp)