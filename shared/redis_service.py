
import sys
sys.path.append('../shared')
from logger import LOGGER
import pika
import json
import time
import os
from rx import Observable, Observer
import threading
import redis
import traceback

class RedisService():
    __logger = None
    __connection = None
    __channel = None
    
    def __init__(self, channel = None):
        self.__logger = LOGGER.getInstance()
        self.__updateChannel(channel)
        self.__createConnection()

    def __createConnection(self):
        for i in range(20):
            try:
                self.__connection = redis.Redis(host='redis', port=6379,decode_responses=True)
                self.__p = self.__connection.pubsub()
            except Exception as e:
                self.__logger.debug("!!!!!!!!!! 1 EXCEPTION !!!!!!!!!")
                self.__logger.debug(str(e))
                self.__logger.debug(traceback.format_exc())
                time.sleep(4)

    def __updateChannel(self, channel):
        self.__channel = channel
    
    def getFlow(self, channel = None):
        if channel != None:
            self.__updateChannel(channel)
        def mainStream(observer):
            def my_handler(message):
                observer.on_next(json.loads(message['data']))
            self.__p.subscribe(**{self.__channel: my_handler})
            thread = self.__p.run_in_thread(sleep_time=0.001)
        return Observable.create(mainStream)

    def updateMessage(self, data, channel = None):
        if channel != None:
            self.__updateChannel(channel)
        try:
            message = json.dumps(data)
            self.__connection.publish(self.__channel, message)
        except Exception as e:
            self.__logger.debug("!!!!!!!!!! 3 EXCEPTION !!!!!!!!!")
            self.__logger.debug(str(e))
            self.__logger.debug(traceback.format_exc())

    def setMSG(self, data, channel):
        message = json.dumps(data)
        self.__connection.set(channel, message)

    def getMSG(self, channel):
        data = self.__connection.get(channel)
        if data != None:
            return json.loads(data)
        else:
            return None
