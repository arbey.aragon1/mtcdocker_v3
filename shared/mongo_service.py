import sys
sys.path.append('../shared')
from pymongo import MongoClient
import pymongo
import os
from logger import LOGGER

class MongoService():
    __instance = None
    __dbRef = None
    __logger = None

    def __init__(self):
        if MongoService.__instance != None:
            raise Exception("Esta clase no es singleton!")
        else:
            MongoService.__instance = self
            client = pymongo.MongoClient(os.environ['DB'])
            self.__dbRef = client[os.environ['MTCDB']]
            self.__logger = LOGGER.getInstance()
            
    @staticmethod
    def getInstance():
        if MongoService.__instance == None:
            MongoService()
        return MongoService.__instance 

    def updateDoc(self, nameTable, nameDocument, dataObj):
        self.__dbRef[nameTable].update({'name': dataObj['name']},   { "$set":  dataObj}, upsert=True )

    def getDoc(self, nameTable):
        return self.__dbRef[nameTable].find()[0]