import sys
sys.path.append('../shared')
sys.path.append('../shared/storage-factory')
from constants import LONG, SHORT, ANY, BUY, SELL
from storage_factory import StorageFactory
from firestore_factory import FirestoreFactory
from logger import LOGGER
from rx import Observable, Observer

class AlgorithmInterface(object):
    __config = None
    __sink = None
    __fs = None
    
    __temporalOrder = None
    __dao = None
    __orderObserver = None
    __orderFlow = None
    logger = None

    def __init__(self, config, sink, configDB = None, marketConfig = None):
        self.logger = LOGGER.getInstance()
        self.__config = config

        self.__dao = StorageFactory.getInstance(configDB)
        self.__fs = FirestoreFactory.getInstance()

        def mainStream(observer):
            self.__orderObserver = observer
        self.__orderFlow = Observable.create(mainStream)

        self.__sink = sink \
            .buffer_with_count(1, self.__config['interval']) \
            .map(lambda s: s[0]) \
            .map(lambda s: self.__mapInput(s)) \
            

    def __mapInput(self, s):
        self.__dao.DATA_IN_SAVE(s)
        self.__temporalOrder={**s}
        return s

    def sendOrder(self, configOrder):
        self.__temporalOrder={**self.__temporalOrder, **configOrder}
        self.__fs.ADD_ORDER('orders', self.__temporalOrder)
        if(configOrder['order'] == SELL):
            self.__dao.SELL(self.__temporalOrder)
            self.__orderObserver.on_next(self.__temporalOrder)

        elif(configOrder['order'] == BUY):
            self.__dao.BUY(self.__temporalOrder)
            self.__orderObserver.on_next(self.__temporalOrder)

    def __mapOutput(self, s):
        self.__dao.DATA_OUT_SAVE(s)
        return s

    def algoTraiding(self, sink):
        raise Exception("Funcion algoTraiding() no implementada!")

    def flowConnect(self):
        return self.algoTraiding(self.__sink) \
            .map(lambda s: self.__mapOutput(s))
    
    def getOrderFlow(self):
        return self.__orderFlow