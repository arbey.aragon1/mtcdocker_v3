# -*- coding: utf-8 -*-
__author__ = 'Arbey Aragon'

class PriceData():
    __name = None
    __ask = None
    __bid = None
    __price = None
    __ids = None
    __time = None
    
    def __init__(self, name='', ask=0, bid=0, ids='', time=''):
        self.__name = name 
        self.__ask = ask
        self.__bid = bid
        self.__price = ask
        self.__ids = ids 
        self.__time = time 

    def getDict(self):
        return {
            'name' :self.__name,
            'price' :self.__price,
            'ask' :self.__ask,
            'bid' :self.__bid,
            'ids' :self.__ids,
            'time' :self.__time,
        }

    def __str__(self):
        return "name: %s, ids: %s, ask: %s, bid: %s, price: %s, time: %s" \
               %(self.__name, self.__ids, self.__ask, self.__bid, self.__price, self.__time)
