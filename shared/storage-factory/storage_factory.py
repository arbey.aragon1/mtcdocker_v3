import sys
import os
sys.path.append('../../shared')
from storage_default import StorageDefault
from storage_production import StorageProduction
from storage_simulation_file import StorageSimulationFile
from storage_simulation_logger import StorageSimulationLogger

ENVIRONMENT=os.environ['ENVIRONMENT']
STAGING=os.environ['STAGING']
PRODUCTION=os.environ['PRODUCTION']
SIMULATION=os.environ['SIMULATION']

class StorageFactory(object):
    @staticmethod
    def getInstance(config):
        if ENVIRONMENT == PRODUCTION:
            #return StorageProduction.getInstance(config)
            return StorageSimulationLogger.getInstance(config)
            #return StorageDefault.getInstance(config)
        elif ENVIRONMENT == STAGING:
            return StorageSimulationLogger.getInstance(config)
        elif ENVIRONMENT == SIMULATION:
            if config['fileOrlogger']:
                return StorageSimulationFile.getInstance(config)
            else:
                return StorageSimulationLogger.getInstance(config)
        else:
            return StorageDefault.getInstance(config)
