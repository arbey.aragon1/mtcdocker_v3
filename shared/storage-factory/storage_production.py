import sys
sys.path.append('../shared')
sys.path.append('../shared/firestore-factory')
from storage_interface import StorageInterface

class StorageProduction(StorageInterface):
    __instance = None

    def __init__(self, config):
        if StorageProduction.__instance != None:
            raise Exception("Esta clase no es singleton!")
        else:
            StorageProduction.__instance = self
           
    @staticmethod
    def getInstance(config):
        if StorageProduction.__instance == None:
            StorageProduction(config)
        return StorageProduction.__instance 
    
    def DATA_IN_SAVE(self, data):
        pass

    def DATA_OUT_SAVE(self, data):
        pass

    def BUY(self, order):
        pass

    def SELL(self, order):
        pass
        
    def STATUS(self, structData):
        pass