import sys
sys.path.append('../../shared')

import constants
from storage_interface import StorageInterface
from storage_simulation_save_files import StorageSimulationSaveFiles
from logger import LOGGER

class StorageSimulationFile(StorageInterface):
    __instance = None

    __dataSave = None
    __dataOutSave = None
    __orderSave = None
    __logger = None
                
    def __init__(self, config):
        if StorageSimulationFile.__instance != None:
            raise Exception("Esta clase no es singleton!")
        else:
            StorageSimulationFile.__instance = self
            self.__logger = LOGGER.getInstance()
            self.setup(config)

    @staticmethod
    def getInstance(config):
        if StorageSimulationFile.__instance == None:
            StorageSimulationFile(config)
        return StorageSimulationFile.__instance 

    def setup(self, config):
        configDataSave = {
            'fileDir': constants.PATH_OUT_SIMULATION_DATA + '/' + config['fileDataName'],
            'loggerName':'data'
        }
        StorageSimulationFile.__dataSave = StorageSimulationSaveFiles(configDataSave)
        
        configDataOutSave = {
            'fileDir': constants.PATH_OUT_SIMULATION_DATA + '/' + config['fileDataNameOut'],
            'loggerName':'dataOut'
        }
        StorageSimulationFile.__dataOutSave = StorageSimulationSaveFiles(configDataOutSave)
        
        configOrderSave = {
            'fileDir': constants.PATH_OUT_SIMULATION_DATA + '/' + config['fileOrderName'],
            'loggerName':'orders'
        }
        StorageSimulationFile.__orderSave = StorageSimulationSaveFiles(configOrderSave)
        
    def DATA_IN_SAVE(self, data):
        StorageSimulationFile.__dataSave.saveRow(data)

    def DATA_OUT_SAVE(self, data):
        StorageSimulationFile.__dataOutSave.saveRow(data)
        
    def BUY(self, order):
        StorageSimulationFile.__orderSave.saveRow(order)
        
    def SELL(self, order):
        StorageSimulationFile.__orderSave.saveRow(order)
        
    def STATUS(self, structData):
        pass
        