import logging
import os

class StorageSimulationSaveFiles(object):
    __config = None
    __writer = None

    def __init__(self, config):
        self.__config = config
        formatter = logging.Formatter('%(message)s')

        def setup_logger(name, log_file, level=logging.INFO):
            handler = logging.FileHandler(log_file,mode='w')        
            handler.setFormatter(formatter)
            logger = logging.getLogger(name)
            logger.setLevel(level)
            logger.addHandler(handler)
            return logger
            
        self.__writer = setup_logger(self.__config['loggerName'], self.__config['fileDir'])

    def saveRow(self,*args, **kwargs):
        self.__writer.info(*args, **kwargs)
