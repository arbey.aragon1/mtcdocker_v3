import sys
sys.path.append('../../shared')

import constants
from storage_interface import StorageInterface
import csv
from logger import LOGGER

class StorageSimulationLogger(StorageInterface):
    __instance = None
    __dataSave = None
    __dataOutSave = None
    __orderSave = None
    __logger = None
                
    def __init__(self, config):
        if StorageSimulationLogger.__instance != None:
            raise Exception("Esta clase no es singleton!")
        else:
            StorageSimulationLogger.__instance = self
            self.__logger = LOGGER.getInstance()

    @staticmethod
    def getInstance(config):
        if StorageSimulationLogger.__instance == None:
            StorageSimulationLogger(config)
        return StorageSimulationLogger.__instance 

    def DATA_IN_SAVE(self, data):
        self.__logger.debug('DATA_IN_SAVE', data)

    def DATA_OUT_SAVE(self, data):
        self.__logger.debug('DATA_OUT_SAVE', data)

    def BUY(self, order):
        self.__logger.debug('BUY', order)

    def SELL(self, order):
        self.__logger.debug('SELL', order)
    
    def STATUS(self, structData):
        self.__logger.debug('STATUS', structData)
