
import sys
sys.path.append('../shared')
from logger import LOGGER
import pika
import json
import time
import os
from rx import Observable, Observer
import threading

class RabbitService():
    __connection = None
    __channel = None
    __logger = None
    __queue = 'default'
    __t1 = None
    __t2 = None
    
    def __init__(self, queue):
        self.__queue = queue
        self.__logger = LOGGER.getInstance()
        self.__connectionCreate()
        
    def __connectionCreate(self):
        for i in range(20):
            try:
                self.__connection = pika.BlockingConnection(
                    pika.ConnectionParameters(
                        host='rabbitmq',                      
                        port=5672,
                        credentials=pika.PlainCredentials(
                            username="admin", password="mypass")
                    )
                )
                break
            except:
                self.__connection = None
                self.__logger.debug('Hubo un error en la conexion '+str(i))
                time.sleep(4)
                
        if(self.__connection == None):
            self.__logger.debug('Tercer intento fallido')
            sys.exit()
        self.__channel = self.__connection.channel()
        self.__logger.debug('Conexion creada')

    def declareQueue(self):        
        self.__channel.queue_declare(queue=self.__queue, durable=True)
    
    def updateMessage(self, data):
        message = json.dumps(data)
        self.__channel.basic_publish(
            exchange='',
            routing_key=self.__queue,
            body=message,
            properties=pika.BasicProperties(
                delivery_mode = 2, # make message persistent
            ))

    def getFlow(self):
        self.__logger.debug('Obtiene el flujo',self.__queue)
        self.__channel.queue_declare(queue=self.__queue, durable=True)
        
        def mainStream(observer):
            def callback(ch, method, properties, body):
                return observer.on_next(json.loads(body))
            
            self.__channel.basic_consume(
                queue=self.__queue, 
                on_message_callback=callback, 
                auto_ack=True)
                
        return Observable.create(mainStream)   
        
    def startConsuming(self):
        self.__t1 = threading.Thread(target=self.__channel.start_consuming)
        self.__t1.start()
        self.__t1.join(0)

    def connectionClose(self):
        self.__connection.close()