
import sys
sys.path.append('../shared')
sys.path.append('../credentials')
from logger import LOGGER
import json
import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore
import time
from firestore_interface import FirestoreInterface
import uuid
from rx import Observable, Observer

@firestore.transactional
def update_in_transaction(transaction, ref, uuid):
    snapshot = ref.get(transaction=transaction)
    if snapshot.get(u'uuid_ms') == 'NOT-MS':
        transaction.update(ref, {
            u'uuid_ms': uuid
        })
        return True
    else:
        return False

class FirestoreProduction(FirestoreInterface):
    __instance = None
    __dbRef = None
    __logger = None
    __UUID_MS = None

    def __init__(self):
        if FirestoreProduction.__instance != None:
            raise Exception("Esta clase no es singleton!")
        else:
            FirestoreProduction.__instance = self
            self.__logger = LOGGER.getInstance()
            cred = credentials.Certificate('../credentials/serviceAccount.json')
            firebase_admin.initialize_app(cred)
            self.__dbRef = firestore.client()
            self.__UUID_MS = str(uuid.uuid4())
            
    @staticmethod
    def getInstance(config={}):
        if FirestoreProduction.__instance == None:
            FirestoreProduction()
        return FirestoreProduction.__instance 

    def ADD_DATA(self, key, data):
        timeDoc=str(round(time.time() * 1000))
        self.__dbRef.collection(key).document(timeDoc).set(data)
        
    def ADD_ORDER(self, key, data):
        timeDoc=str(round(time.time() * 1000))
        self.__dbRef.collection(key).document(timeDoc).set(data)
        
    def GET_DATA_BY_LIMIT(self, key, order, lim):
        self.__logger.debug('GET_DATA_BY_LIMIT')
        query = self.__dbRef.collection(key).order_by(order, direction=firestore.Query.DESCENDING).limit(lim)
        return query.stream()

    def GET_TASK_QUEUE(self):
        def mainStream(observer):
            doc_ref = self.__dbRef \
                .collection("microservice") \
                .document("echo") \
                .collection("task")

            def on_snapshot(doc_snapshot, changes, read_time):
                for i, doc in enumerate(doc_snapshot):
                    data=doc.to_dict()
                    if (update_in_transaction(self.__dbRef.transaction(), 
                        doc_ref.document(doc.id), 
                        self.__UUID_MS)):
                        observer.on_next({**doc.to_dict(), 'task_id': doc.id})
                        #self.__logger.debug(u'Received document snapshot: {} {} {}'.format(i, data['time'], data))
                    
            doc_watch = doc_ref \
                .where(u'uuid_ms', u'==', u'NOT-MS') \
                .order_by('time', direction=firestore.Query.ASCENDING) \
                .limit(1) \
                .on_snapshot(on_snapshot)

        return Observable.create(mainStream)

    def RESPONSE(self, req, resp):
        batch = self.__dbRef.batch()

        batch.set(self.__dbRef \
            .collection("microservice") \
            .document("echo") \
            .collection(req['uid_user']) \
            .document(req['uuid']), resp)

        batch.delete(self.__dbRef \
            .collection("microservice") \
            .document("echo") \
            .collection("task") \
            .document(req['task_id']))

        batch.commit()

    def START(self):
        pass

