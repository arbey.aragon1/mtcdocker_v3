import sys
import os
sys.path.append('../../shared')

from firestore_production import FirestoreProduction
from firestore_logger import FirestoreLogger

ENVIRONMENT=os.environ['ENVIRONMENT']
STAGING=os.environ['STAGING']
PRODUCTION=os.environ['PRODUCTION']
SIMULATION=os.environ['SIMULATION']

class FirestoreFactory(object):
    @staticmethod
    def getInstance(config={}):
        if ENVIRONMENT == PRODUCTION:
            return FirestoreProduction.getInstance(config)
        else:
            return FirestoreLogger.getInstance(config)
