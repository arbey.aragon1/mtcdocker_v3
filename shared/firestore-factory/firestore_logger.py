
import sys
sys.path.append('../shared')
sys.path.append('../credentials')
from logger import LOGGER
import json
import time
from firestore_interface import FirestoreInterface

class FirestoreLogger(FirestoreInterface):
    __instance = None
    __dbRef = None
    __logger = None

    def __init__(self):
        if FirestoreLogger.__instance != None:
            raise Exception("Esta clase no es singleton!")
        else:
            FirestoreLogger.__instance = self
            self.__logger = LOGGER.getInstance()
            
    @staticmethod
    def getInstance(config):
        if FirestoreLogger.__instance == None:
            FirestoreLogger()
        return FirestoreLogger.__instance 

    def ADD_DATA(self, key, data):
        timeDoc=str(round(time.time() * 1000))
        self.__logger.debug('firestore data: ',key, data)

    def ADD_ORDER(self, key, data):
        timeDoc=str(round(time.time() * 1000))
        self.__logger.debug('firestore order: ',key, data)

    def GET_DATA_BY_LIMIT(self, key, order, lim):
        return []

    