# External libs
from rx import Observable, Observer
import random
import numpy as np
import pandas as pd

def getKey(item):
    return item[0]

def empty2(dateInit, dateEnd):
    index = pd.date_range(dateInit, dateEnd, freq='T')
    series = pd.DataFrame(index=index)
    return series

def empty(dateInit):
    dateEnd = str(pd.to_datetime(dateInit) +
                  dt.timedelta(hours=24) - dt.timedelta(minutes=1))
    return empty2(dateInit, dateEnd)


def streamRandomWalk(mlSeconds=1000, initVal=50, mu=1, sigma=0.5, limitData=500):
    return Observable.interval(mlSeconds) \
        .map(lambda s: {'index': 0,
            'price': 0
        }).start_with({'index': 0,
            'price': initVal
        }) \
        .scan(lambda x, y: {'index': x['index'] + 1,
            'price': x['price'] + random.choice([-1, 1])*mu + np.random.normal(0, sigma)
        })

def pad(x):
    date = str(x[0])[:-2]
    time = str(x[1])[:-2]
    temp = date+'0'*(6-len(time))+time
    return temp[:4]+'-'+temp[4:6]+'-'+temp[6:8]+' '+temp[8:10]+':'+temp[10:12]+':'+temp[12:14]
   
def streamHistData(fileName='dataToSim.csv', minDelta=None, mlSeconds=None):
    fileName = '../data-simulation/'+fileName

    def mainStream(observer):        
        df=pd.read_csv(fileName, delimiter=';', decimal='.', header=None)
        df.index=pd.to_datetime(df[0])
        del df[0]

        day = counter = 0
        for dateStartDay, groupD in df.groupby(pd.Grouper(freq='D')):
            #if(pd.to_datetime(dateStartDay).dayofweek <= 4 and groupD.shape[0]):
            #    #ddf = empty2(groupD.head(1).index[0],groupD.tail(1).index[0])
            #    ddf=groupD
            #    #ddf[1] = groupD[1]
            #    #ddf = ddf.fillna(method='ffill').fillna(method='bfill')
            #
            #    for index, row in ddf.iterrows():
            #        val = {
            #            'counter': counter,
            #            'day': day,
            #            'time': str(index),
            #            'price': row[1]
            #        }
            #        observer.on_next(val)
            #        counter = counter + 1
            #    day = day+1
            ##############################
            for index, row in groupD.iterrows():
                val = {
                    'counter': counter,
                    'day': day,
                    'time': str(index),
                    'price': row[1]
                }
                observer.on_next(val)
                counter = counter + 1
            day = day+1
            #################################
        observer.on_completed()
    ys = None

    def deleteCounter(y):
        del y['counter']
        return y

    if(minDelta == None): 
        ys = Observable.create(mainStream)
    else:
        ys = Observable.create(mainStream) \
            .filter(lambda s: s['counter'] % minDelta == 0) \
            .map(lambda s: deleteCounter(s))

    def addProp(x, y):
        #y['id'] = x
        return y

    source = None
    if(mlSeconds == None):
        source = ys
    else:
        source = Observable.interval(mlSeconds).zip(ys, lambda x, y: addProp(x, y))
    return source
