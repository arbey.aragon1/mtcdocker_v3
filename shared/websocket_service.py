
import sys
sys.path.append('../shared')
from logger import LOGGER
import pika
import json
import time
import os
from rx import Observable, Observer
import threading
import redis
import traceback
from simple_websocket import SimpleWebSocket
import tornado.ioloop
import tornado.web
import tornado.websocket
import tornado.httpserver
import tornado.options

class WebsocketService():
    __logger = None
    __instance = None
    
    def __init__(self, channel = None):
        if WebsocketService.__instance != None:
            raise Exception("Esta clase no es singleton!")
        else:
            WebsocketService.__instance = self
            self.__logger = LOGGER.getInstance()

    @staticmethod
    def getInstance(config={}):
        if WebsocketService.__instance == None:
            WebsocketService()
        return WebsocketService.__instance 

    def GET_TASK_QUEUE(self):
        return SimpleWebSocket.GET_TASK_QUEUE()

    def RESPONSE(self, req, resp):
        SimpleWebSocket.RESPONSE(req, resp)

    def START(self):
        tornado.options.parse_command_line()
        app = tornado.web.Application([
            (r"/", SimpleWebSocket)
        ])
        server = tornado.httpserver.HTTPServer(app)
        server.listen(5678,"0.0.0.0")
        tornado.ioloop.IOLoop.current().start()


