
import sys
sys.path.append('../shared')
from logger import LOGGER
import pika
import json
import time
import os
from rx import Observable, Observer

class RabbitPubSubService():
    __connection = None
    __channel = None
    __logger = None
    __queue = None
    
    def __init__(self, queue):
        self.__logger = LOGGER.getInstance()
        self.__connectionCreate()
        
    def __connectionCreate(self):
        for i in range(20):
            try:
                self.__connection = pika.BlockingConnection(
                    pika.ConnectionParameters(
                        host='rabbitmq',                      
                        port=5672,
                        credentials=pika.PlainCredentials(
                            username="admin", password="mypass")
                    )
                )
                break
            except:
                self.__connection = None
                self.__logger.debug('Hubo un error en la conexion '+str(i))
                time.sleep(4)
                
        if(self.__connection == None):
            self.__logger.debug('Tercer intento fallido')
            sys.exit()
        self.__channel = self.__connection.channel()
        self.__logger.debug('Conexion creada')

    def exchangeDeclare(self):
        self.__channel.exchange_declare(exchange='logs', exchange_type='fanout')
    
    def updateMessage(self, data):
        message = json.dumps(data)
        self.__channel.basic_publish(
            exchange='logs', 
            routing_key='', 
            body=message)

    def getFlow(self):
        self.__logger.debug('Obtiene el flujo',self.__queue)
        self.exchangeDeclare()
        self.__queue = self.__channel.queue_declare('', exclusive=True).method.queue
        self.__channel.queue_bind(exchange='logs', queue=self.__queue)
        def mainStream(observer):
            def callback(ch, method, properties, body):
                return observer.on_next(json.loads(body))
            
            self.__channel.basic_consume(
                queue=self.__queue, 
                on_message_callback=callback, 
                auto_ack=True)
        return Observable.create(mainStream)   
        
    def startConsuming(self):
        self.__logger.debug(' [*] Waiting for messages. To exit press CTRL+C')
        self.__channel.start_consuming()

    def connectionClose(self):
        self.__connection.close()